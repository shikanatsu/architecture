package top.shikanatsu.blog.lock.jol;


import org.openjdk.jol.info.ClassLayout;
import top.shikanatsu.blog.lock.obj.A;

import static java.lang.System.out;

/**
 * @className: JOLExample1JOLExample1
 * @description: TODO 类描述
 * @author: shikanatsu
 * @date: 2022/1/11
 **/
public class JOLExample1 {

//    public static void main(String[] args) throws Exception {
//        out.println(VM.current().details());
//        out.println(ClassLayout.parseClass(A.class).toPrintable());
//
//    }

    public static void main(String[] args) throws Exception {
         A a = new A();
         out.println("befor hash");
         //没有计算HASHCODE之前的对象头
        out.println(ClassLayout.parseInstance(a).toPrintable());
        //JVM 计算的hashcode
        out.println("jvm‐‐‐‐‐‐‐‐‐‐‐‐"+Integer.toHexString(a.hashCode()));
        HashUtil.countHash(a);//当计算完hashcode之后，我们可以查看对象头的信息变化
        out.println("after hash");
        out.println(ClassLayout.parseInstance(a).toPrintable()); }
    }

