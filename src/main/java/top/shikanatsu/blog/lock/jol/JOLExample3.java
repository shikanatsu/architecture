package top.shikanatsu.blog.lock.jol;

import org.openjdk.jol.info.ClassLayout;
import top.shikanatsu.blog.lock.obj.B;

import static java.lang.System.out;

/**
 * @className: JOLExample2
 * @description: TODO 类描述
 * @author: shikanatsu
 * @date: 2022/1/11
 **/
public class JOLExample3 {

    public static void main(String[] args) throws Exception {
        //-XX:+UseBiasedLocking -XX:BiasedLockingStartupDelay=0
        Thread.sleep(5000);
        B b = new B();
        out.println("befor lock");
        out.println(ClassLayout.parseInstance(b).toPrintable());
        synchronized (b){
            out.println("lock ing");
            out.println(ClassLayout.parseInstance(b).toPrintable());
        }
        out.println("after lock");
        out.println(ClassLayout.parseInstance(b).toPrintable());
    }
}
