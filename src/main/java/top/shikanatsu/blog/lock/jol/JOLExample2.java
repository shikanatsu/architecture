package top.shikanatsu.blog.lock.jol;

import org.openjdk.jol.info.ClassLayout;
import top.shikanatsu.blog.lock.obj.B;

import static java.lang.System.out;

/**
 * @className: JOLExample2
 * @description: TODO 类描述
 * @author: shikanatsu
 * @date: 2022/1/11
 **/
public class JOLExample2 {

    public static void main(String[] args) throws Exception {
        B b = new B();
        out.println("befor hash");
        //没有计算HASHCODE之前的对象头
        out.println(ClassLayout.parseInstance(b).toPrintable());
        //JVM 计算的hashcode
        out.println("jvm------------0x"+Integer.toHexString(b.hashCode()));
        HashUtil.countHash(b);
        //当计算完hashcode之后，我们可以查看对象头的信息变化
        out.println("after hash");
        out.println(ClassLayout.parseInstance(b).toPrintable());

    }
}
