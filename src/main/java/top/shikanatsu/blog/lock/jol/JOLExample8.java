package top.shikanatsu.blog.lock.jol;

import org.openjdk.jol.info.ClassLayout;
import top.shikanatsu.blog.lock.obj.A;

import static java.lang.System.out;

/**
 * @className: JOLExample6
 * @author: shikanatsu
 * @date: 2022/1/11
 **/
public class JOLExample8 {

    static A a;
    public static void main(String[] args) throws Exception {
        Thread.sleep(5000);
        a= new A();
        a.hashCode();
        out.println("befor lock");
        out.println(ClassLayout.parseInstance(a).toPrintable());
        synchronized (a){
            out.println("lock ing");
            out.println(ClassLayout.parseInstance(a).toPrintable());
        }
        out.println("after lock");
        out.println(ClassLayout.parseInstance(a).toPrintable());
    }
}
