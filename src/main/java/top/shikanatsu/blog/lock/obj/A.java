package top.shikanatsu.blog.lock.obj;

/**
 * @className: A
 * @description:
 * @author: shikanatsu
 * @date: 2022/1/11
 **/
public class A {
    //占一个字节的boolean字段
//    boolean flag = false;

    int i=0;

    public synchronized void parse(){
        i++;

    }
    //JOLExample6.countDownLatch.countDown();

}
